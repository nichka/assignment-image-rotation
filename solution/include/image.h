#ifndef IMAGE
#define IMAGE
#include <stdint.h>

struct pixel { uint8_t b, g, r; };

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image image_init (uint32_t height, uint32_t width);

void image_destroy(struct image* image);

#endif
