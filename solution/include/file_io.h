#ifndef IMAGE_H_FILE_IO_H
#define IMAGE_H_FILE_IO_H
#include "bmp_file_work.h"
#include "image.h"
#include <stdio.h>


enum read_status open_file(FILE** file, const char* filepath, const char* mode);

void close_file( FILE* file );

#endif
