#include "file_io.h"
#include "bmp_file_work.h"
#include "image.h"
#include "rotation.h"
#include <stdio.h>


int main(int argc, char** argv) {
    if (argc < 3) {
        fprintf(stderr, "Wrong number of args");
        return READ_ERROR;
    }
    FILE *in_file, *out_file;
    struct image img = {0};
    enum read_status in = open_file(&in_file, argv[1], "rb");
    enum read_status out = open_file(&out_file, argv[2], "wb");
    if (in != READ_OK || out != READ_OK) {
        fprintf(stderr, "Couldn't open file");
        return READ_INVALID_DATA;
    }

    if (from_bmp(in_file, &img) != READ_OK) {
        fprintf(stderr, "Reading of file was failed");
        return READ_ERROR;
    }

    struct image rot_image = rotate(&img);

    if (to_bmp(out_file, &rot_image) != WRITE_OK) {
        fprintf(stderr, "Writing in file was failed");
        return WRITE_ERROR;
    }
    
    close_file(in_file);
    close_file(out_file);

    image_destroy(&img);
    image_destroy(&rot_image);

    return 0;
}
