#include "bmp_file_work.h"
#include "image.h"
#include <stdlib.h>


struct __attribute__((packed)) bmp_header
{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

struct bmp_header get_header(struct image const* img, size_t const padding) {
    struct bmp_header header = {
            .bfType = 0X4D42,
            .bfileSize = img->height * (img->width * 3 + padding) + sizeof(struct bmp_header),
            .bfReserved = 0,
            .bOffBits = 54,
            .biSize = 40,
            .biWidth = img->width,
            .biHeight = img->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = img->height * (img->width * 3 + padding),
            .biXPelsPerMeter = 2834,
            .biYPelsPerMeter = 2834,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return header;
}

uint32_t get_padding(uint32_t width) {
    return (4 - (width * 3) % 4) % 4;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1) {return READ_INVALID_HEADER;}
    if (header.bfType != 0X4D42 || header.bfReserved != 0 || header.biSize != 40 || header.biBitCount != 24) {
        return READ_INVALID_SIGNATURE;
    }

    *img = image_init(header.biHeight, header.biWidth);
    uint8_t const padding = get_padding(img->width);
    for (size_t i = 0; i < img->height; i++) {
        if (fread(&(img -> data[i * img -> width]), sizeof(struct pixel), header.biWidth, in) != img->width || fseek(in, padding, SEEK_CUR) != 0) {
            free(img->data);
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    size_t const padding = get_padding(img->width);
    char const clear_b[] = {0, 0, 0};
    struct bmp_header const header = get_header(img, padding);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) {
        return WRITE_ERROR;
    }
    for (size_t i = 0; i < img->height; i++) {
        if (fwrite(img->data + img->width * i, sizeof(struct pixel), img->width, out) != img->width){
            return WRITE_ERROR;
        }
        if (fwrite(clear_b, sizeof(char), padding, out) != padding){
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
