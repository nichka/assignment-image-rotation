#include "image.h"
#include <stdlib.h>


struct image image_init(uint32_t height, uint32_t width) {
    return (struct image) {
        .width = width,
        .height = height,
        .data = malloc(sizeof(struct pixel) * height * width)
    };
}

void image_destroy(struct image* image) {
    free(image->data);
}
