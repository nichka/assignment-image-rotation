#include "file_io.h"


enum read_status open_file(FILE** file, const char* filepath, const char* mode) {
    *file = fopen(filepath, mode);
    if (!*file) {
        return READ_INVALID_DATA;
    }
    return READ_OK;
}

void close_file( FILE* file ) {
    fclose(file);
}
