#include "image.h"
#include "rotation.h"
#include <stdlib.h>

struct image rotate( struct image const* source ) {
    struct image rot_image = {
            .height = source->width,
            .width = source->height,
            .data = malloc(sizeof(struct pixel) * source->height * source->width)
    };

    for (size_t i = 0; i < source->width; i++){
        for (size_t j = 0; j < source->height; j++){
            rot_image.data[i * rot_image.width + j] = source->data[source->width * (rot_image.width - j - 1) + i];
        }
    }

    return rot_image;
}
